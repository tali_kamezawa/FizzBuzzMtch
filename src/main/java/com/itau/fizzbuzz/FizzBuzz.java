package com.itau.fizzbuzz;

public class FizzBuzz {

	public static String fizzBuzz(int i) {
		//		if (i % 3 == 0) {
		//			return "fizz";
		//		}
		//		return null;
		if ((i % 3 ==0) && (i % 5 ==0))  {
			return "FizzBuzz";
		}

		if (i % 5 == 0) {
			return "Buzz";
		}

		if ( i % 3 == 0) {
			return "Fizz";

		}

		return Integer.toString(i);


	}

	public static String imprimeValores(int number) {

		StringBuilder str = new StringBuilder();
		String ret = "";
		for(int i=1; i<=number ; i++) {
			if (i == number) {
				ret = fizzBuzz(i) + ".";
			}
			else {
				ret = fizzBuzz(i) + ", ";
			}
			str.append(ret);
		}

		return str.toString();
	}



}
